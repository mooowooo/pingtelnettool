一、功能介绍
PingTelnetTool程式用于检测指定服务器地址的网络连接是否正常，对网络连接不正常的服务器使用telnet远程连接进行重启操作。

二、文件组成
1- README.TXT：自述文档
2- Ping-Telnet.exe：程式可执行文件
3- config.ini：配置文档，用于配置目标IP，接收telnet指令的IP，telnet用户名，密码，以及telnet指令
4- Log.log：日志文件，保存程式每次运行过程的结果

三、使用方法
保证config.ini与Ping-Telnet.exe以及相关的文件和文件夹在同一个目录下，直接运行Ping-Telnet.exe即可。如需定时操作，也可以为其排JOB自动执行。

四、残留问题
1- 暂时无法关闭Log记录，只能通过修改源代码取消Log记录功能。
2- 暂时无法自定义Log.log和config.ini文件的读取路径。
3- 暂时无法修改ping的次数。

五、修改记录
-- Date --  -- Author --  -- Version --   ----- Deatil -----
2013-08-28   Jux           20130828        通过ping操作进行检测，检测期间将5次Ping操作，当Ping通时，立即退出程式，并记录LOG。当5次均不通时，将通过telnet远程连接执行定义的操作
2013-08-29   Jux           20130829        修改逻辑，一直ping，遇到不通的情况则发送指令，等待1分20秒后再次做Ping操作。