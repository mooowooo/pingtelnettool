﻿import os, sys, time, telnetlib

fLog = 'Log.log'

def main(configFilePath):
    try:
        getIP = open(configFilePath,'r')
        
        # should be read(), if use readlines() will result some errors.
        # split() must use sign '\n' to split the line, other wise it will drop some null/'' elements.
        IP = getIP.read().split('\n')
        getIP.close()

#        if pingFun(IP[1]) <> True:
#            telnetFun(configFilePath)
        while True:
            while pingFun(IP[1]):
                print time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))+'\t\tPing OK'
                time.sleep(1)    # once ping per-second
            telnetFun(configFilePath)
            time.sleep(80)
    except IOError, e:
        print '''\n
        -------------------- ERROR --------------------\n
        Could not found the ConfigFile !\n
        Please read the ReadMe.txt\n
        make sure the PATH of ConfigFile is right AND TRY AGAIN !\n
        This error has been writen into the Log File.\n
        -----------------------------------------------'''

        # write the log file.
        getIP = open(fLog,'a')
        strLog = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
        strLog = strLog + '\t\t IO Error\t\tCould not open ConfigFile.\n'
        getIP.write(strLog)
    finally:
        getIP.close()

def writeLog(logFilePath,strLog):
    strLog = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))+'\t\t'+strLog
    wrtLog = open(logFilePath, 'a')
    wrtLog.write(strLog)
    wrtLog.close()

def pingFun(strIP):
    failTime = 0
    while failTime < 5:
#        ping = os.system('ping -n 1 ' + strIP)    # For Windows use
        ping = os.system('ping -c 1 ' + strIP)    # For *nix use
        if ping == 0:
            #strLog = ' Ping OK\t\tPing %s OK.\n' % strIP
            #writeLog('Log.log',strLog)
            return True
        else:
            failTime = failTime + 1
            time.sleep(1)
            
        if failTime == 5:
            strLog = 'Ping Fail\t\tPingt %s Fail.\n' % strIP
            writeLog(fLog, strLog)
            return False

def telnetFun(localPath):
    print '1---\tTelnet function start.'
    # read config file
    configFile = open(localPath,'r')
    try:
        fileInfo = configFile.read().split('\n')
    finally:
        configFile.close()
    # add command 'quit' to make sure the telnet can be quit before close the telnet stream.
    fileInfo.append('quit')
#    time.sleep(1)

    host = fileInfo[2]
    port = fileInfo[3]
    username = fileInfo[4]
    pwd = fileInfo[5]
    # read config file fin.
    print '2---\tRead config file finished.'
#    time.sleep(1)

    tn = telnetlib.Telnet(host, port, 10)
    #tn = telnetlib.Telnet(host)

    # Login part
    if username <> '':
        tn.read_until(b'Login: ')
        tn.write(username.encode('UTF-8') + b'\r\n')
    tn.read_until(b'Password: ')
    tn.write(pwd.encode('UTF-8') + b'\r\n')
    # Login part fin.
    print '3---\tLogin success.'
#    time.sleep(1)

    # command part
    cmdLoop = len(fileInfo)-8
    command = fileInfo[7:]
    loopTime = 0
    while cmdLoop >= loopTime:
        tn.read_until(b'>')
        print '4-%d-\t%s.' % (loopTime, command[loopTime])
        if command[loopTime] <> '':
            tn.write(command[loopTime].encode('UTF-8') + b'\r\n')
            time.sleep(1)
        if command[loopTime] == 'quit':
            break
        loopTime = loopTime+1

    # write result in files.
    # TODO: write log files.

    print '5---\tLogout success.'
#    time.sleep(1)
    tn.close()    # close the stream
    print '6-1-\tConnection close success.'
    print '6-2-\tTelnet function finished.'
    strLog = 'CommandsOK\t\tCommands Excute OK.\n'
    writeLog(fLog, strLog)
    time.sleep(1)

if __name__ == '__main__':
    main('config.ini')